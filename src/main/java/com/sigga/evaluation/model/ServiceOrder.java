package com.sigga.evaluation.model;

import com.sigga.evaluation.enumeration.ContactType;
import com.sigga.evaluation.enumeration.ServiceOrderType;
import com.sigga.evaluation.enumeration.StatusType;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "TB_SERVICE_ORDER", schema = "ORDER_OWNER")
public class ServiceOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "OID_SERVICE_ORDER", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long oidServiceOrder;

    @Column(name = "CUSTOMER_NAME")
    private String customerName;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "DOCUMENT")
    private String taxIdNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "TP_CONTACT")
    private ContactType contactType;

    @Column(name = "CONTACT")
    private String contact;

    @Enumerated(EnumType.STRING)
    @Column(name = "TP_SERVICE_ORDER")
    private ServiceOrderType serviceOrderType;

    @Column(name = "TX_DESCRIPTION")
    private String jobDescription;

    @Column(name = "DT_OPEN")
    private LocalDateTime openDate;

    @Column(name = "DT_CLOSING")
    private LocalDateTime closingDate;

    @Column(name = "RESPONSIBLE")
    private String responsible;

    @Enumerated(EnumType.STRING)
    @Column(name = "TP_STATUS")
    private StatusType statusType;
}
