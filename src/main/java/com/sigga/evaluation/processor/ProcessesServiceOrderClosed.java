package com.sigga.evaluation.processor;

import com.sigga.evaluation.enumeration.StatusType;
import com.sigga.evaluation.model.ServiceOrder;
import com.sigga.evaluation.service.ServiceOrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
@Slf4j
public class ProcessesServiceOrderClosed implements ProcessesServiceOrder {

    private final ServiceOrderService service;

    @Override
    public void sues(ServiceOrder serviceOrder) {
        log.info("Processando ordem de serviço: " + serviceOrder.getOidServiceOrder());
        serviceOrder.setClosingDate(LocalDateTime.now());
        service.saveServiceOrder(serviceOrder);
    }

    @Override
    public boolean supports(StatusType status) {
        return status == StatusType.CLOSED;
    }
}
