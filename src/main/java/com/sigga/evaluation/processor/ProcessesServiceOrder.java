package com.sigga.evaluation.processor;

import com.sigga.evaluation.enumeration.StatusType;
import com.sigga.evaluation.model.ServiceOrder;

public interface ProcessesServiceOrder {
    void sues(ServiceOrder serviceOrder);

    boolean supports(StatusType status);
}
