package com.sigga.evaluation.exception;

import lombok.Getter;

@Getter
public class HttpClientException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private int httpStatus;

    public HttpClientException(int status, String message) {
        super(message);
        this.httpStatus = status;
    }
}
