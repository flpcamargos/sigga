package com.sigga.evaluation.exception;

public class ContactTypeValidationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ContactTypeValidationException(String message) {
        super(message);
    }
}
