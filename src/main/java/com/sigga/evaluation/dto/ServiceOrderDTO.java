package com.sigga.evaluation.dto;

import com.sigga.evaluation.enumeration.ContactType;
import com.sigga.evaluation.enumeration.ServiceOrderType;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ServiceOrderDTO {

    @Size(min = 3, max = 100, message = "O nome do cliente deve ter entre 3 e 100 caracteres.")
    @NotBlank(message = "Preenchimento do nome é obrigatório.")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Nome do cliente não pode conter números ou caracteres especiais.")
    private String customerName;

    @Size(min = 11, max = 14, message = "O documento do cliente deve ter entre 11 e 14 caracteres.")
    @NotBlank(message = "Preenchimento do documento é obrigatório.")
    @Pattern(regexp = "^[0-9]+$", message = "Documento não pode conter letras ou caracteres especiais.")
    private String taxIdNumber;

    @Size(min = 8, max = 8, message = "O codigo postal ter 8 caracteres.")
    @NotBlank(message = "Preenchimento do codigo postal é obrigatório.")
    @Pattern(regexp = "^[0-9]+$", message = "Codigo postal não pode conter letras ou caracteres especiais.")
    private String postalCode;

    @NotNull(message = "Preenchimento do tipo de contato é obrigatório.")
    private ContactType contactType;

    @Size(min = 1, max = 100, message = "O contato do cliente deve ter entre 3 e 100 caracteres.")
    @NotBlank(message = "Preenchimento do contato é obrigatório.")
    private String contact;

    @NotNull(message = "Preenchimento do tipo da ordem de serviço é obrigatório.")
    private ServiceOrderType serviceOrderType;

    @Size(min = 10, max = 250, message = "A descrição da ordem de serviço deve ter entre 10 e 250 caracteres.")
    @NotBlank(message = "Preenchimento da descrição da ordem de serviço é obrigatório.")
    private String jobDescription;
}
