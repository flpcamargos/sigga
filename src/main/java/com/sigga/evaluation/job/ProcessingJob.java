package com.sigga.evaluation.job;

import com.sigga.evaluation.service.ProcessingServiceOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class ProcessingJob implements Job {

    private final ProcessingServiceOrderService pollService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Executando job para processar as ordens de serviço.");
        pollService.processingOrders();
    }
}
