package com.sigga.evaluation.util;

public class ConstantsUtil {
    public static final String MAIL_INVALID = "Email inválido.";
    public static final String PHONE_INVALID = "Telefone inválido.";
    public static final String REGEX_PHONE = "^\\+[1-9][0-9]\\d{1,14}$";
    public static final String REGEX_MAIL = "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`\\{|\\}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]" +
            "{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";
    public static final String RESPONSIBLE = "Felipe.Ribeiro";

    private ConstantsUtil() {
    }
}
