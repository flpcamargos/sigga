package com.sigga.evaluation.enumeration;

import com.sigga.evaluation.exception.ContactTypeValidationException;
import com.sigga.evaluation.util.ConstantsUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum ContactType {
    PHONE(ConstantsUtil.PHONE_INVALID) {
        public boolean isValid(String contact) {
            return validatePattern(contact, ConstantsUtil.REGEX_PHONE);
        }
    },
    EMAIL(ConstantsUtil.MAIL_INVALID) {
        public boolean isValid(String contact) {
            return validatePattern(contact, ConstantsUtil.REGEX_MAIL);
        }
    };

    private String errorMessage;

    protected abstract boolean isValid(String contact);

    ContactType(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void validate(String contact) {
        if (!this.isValid(contact)) {
            throw new ContactTypeValidationException(errorMessage);
        }
    }

    protected boolean validatePattern(String value, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(value);
        return m.matches();
    }
}
