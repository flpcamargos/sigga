package com.sigga.evaluation.enumeration;

public enum ServiceOrderType {
    CORRECTIVE_MAINTENANCE,
    SERVICE_REQUEST,
    FIXED_TASKS,
    PREVENTIVE_MAINTENANCE,
    EVENTS;
}
