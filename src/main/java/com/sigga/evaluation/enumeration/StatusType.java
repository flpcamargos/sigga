package com.sigga.evaluation.enumeration;

public enum StatusType {
    OPEN,
    CLOSED,
    PROCESSING,
    PAUSED,
    WAITING_APPROVAL,
    READY_RUN;
}
