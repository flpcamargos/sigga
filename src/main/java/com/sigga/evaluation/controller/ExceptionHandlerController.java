package com.sigga.evaluation.controller;

import com.sigga.evaluation.exception.ContactTypeValidationException;
import com.sigga.evaluation.exception.HttpClientException;
import com.sigga.evaluation.exception.StandardError;
import com.sigga.evaluation.exception.ValidationError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionHandlerController {

    public static final String MSG_VALIDACAO = "Erro de validação";
    public static final String MSG_VALIDACAO_API = "Erro api externa.";

    @ExceptionHandler(ContactTypeValidationException.class)
    public ResponseEntity<StandardError> getMailValidatorException(ContactTypeValidationException e, HttpServletRequest request) {
        StandardError error = new StandardError(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                e.getMessage(), e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(error);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StandardError> validation(MethodArgumentNotValidException e, HttpServletRequest request) {
        ValidationError err = new ValidationError(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                MSG_VALIDACAO, e.getMessage(), request.getRequestURI());
        for (FieldError x : e.getBindingResult().getFieldErrors()) {
            err.addError(x.getField(), x.getDefaultMessage());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<StandardError> validation(ConstraintViolationException e, HttpServletRequest request) {
        ValidationError err = new ValidationError(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                MSG_VALIDACAO, e.getMessage(), request.getRequestURI());
        e.getConstraintViolations().stream().forEach(constraintViolation -> {
            err.addError(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
        });
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<StandardError> getMissingRequestHeaderException(MissingRequestHeaderException e, HttpServletRequest request) {
        StandardError err = new StandardError(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                MSG_VALIDACAO, e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<StandardError> getHttpMessageNotReadableException(HttpMessageNotReadableException e, HttpServletRequest request) {
        StandardError err = new StandardError(LocalDateTime.now(), HttpStatus.BAD_REQUEST.value(),
                MSG_VALIDACAO, e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(HttpClientException.class)
    public ResponseEntity<StandardError> getHttpClientException(HttpClientException e, HttpServletRequest request) {
        StandardError err = new StandardError(LocalDateTime.now(), e.getHttpStatus(),
                MSG_VALIDACAO_API, e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(e.getHttpStatus()).body(err);
    }
}
