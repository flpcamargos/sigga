package com.sigga.evaluation.controller;

import com.sigga.evaluation.dto.ServiceOrderDTO;
import com.sigga.evaluation.service.ServiceOrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/v1/service-orders")
public class ServiceOrderController {

    private final ServiceOrderService service;

    @PostMapping
    @ApiOperation(value = "Criar uma ordem de serviço.")
    @ApiResponses({@ApiResponse(code = 201, message = "Ordem de serviço criada com sucesso."),
            @ApiResponse(code = 400, message = "Erro validação."),
            @ApiResponse(code = 500, message = "Erro interno.")})
    public ResponseEntity createServiceOrder(@RequestBody @Valid ServiceOrderDTO serviceOrderDTO) {
        service.createServiceOrder(serviceOrderDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
