package com.sigga.evaluation.client;

import com.sigga.evaluation.exception.HttpClientException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ApiErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        return new HttpClientException(response.status(), response.reason());
    }
}
