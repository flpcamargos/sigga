package com.sigga.evaluation.client.address;

import com.sigga.evaluation.dto.AddressDTO;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RefreshScope
@FeignClient(name = "AddressClient", url = "https://viacep.com.br/ws")
public interface AddressClient {

    @GetMapping(value = "{cep}/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    AddressDTO getAddress(@PathVariable(value = "cep") String cep);
}
