package com.sigga.evaluation.service;

import com.sigga.evaluation.client.address.AddressClient;
import com.sigga.evaluation.dto.AddressDTO;
import com.sigga.evaluation.dto.ServiceOrderDTO;
import com.sigga.evaluation.enumeration.StatusType;
import com.sigga.evaluation.model.ServiceOrder;
import com.sigga.evaluation.repository.ServiceOrderRepository;
import com.sigga.evaluation.util.ConstantsUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ServiceOrderService {

    private final ModelMapper mapper;
    private final ServiceOrderRepository repository;
    private final AddressClient client;

    public void createServiceOrder(ServiceOrderDTO serviceOrderDTO) {
        serviceOrderDTO.getContactType().validate(serviceOrderDTO.getContact());
        ServiceOrder serviceOrder = mapper.map(serviceOrderDTO, ServiceOrder.class);
        completeServiceOrder(serviceOrderDTO, serviceOrder);
        repository.save(serviceOrder);
    }

    public List<ServiceOrder> findAllServiceOrders() {
        return repository.findAll();
    }

    public void saveServiceOrder(ServiceOrder serviceOrder) {
        repository.save(serviceOrder);
    }

    private void completeServiceOrder(ServiceOrderDTO serviceOrderDTO, ServiceOrder serviceOrder) {
        AddressDTO addressDTO = client.getAddress(serviceOrderDTO.getPostalCode());
        log.info("Endereço: " + addressDTO);
        serviceOrder.setAddress(addressDTO.getLogradouro());
        serviceOrder.setOpenDate(LocalDateTime.now());
        serviceOrder.setClosingDate(LocalDateTime.now().plusDays(1));
        serviceOrder.setResponsible(ConstantsUtil.RESPONSIBLE);
        serviceOrder.setStatusType(StatusType.OPEN);
    }
}
