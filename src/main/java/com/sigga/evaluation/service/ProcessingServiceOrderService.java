package com.sigga.evaluation.service;

import com.sigga.evaluation.model.ServiceOrder;
import com.sigga.evaluation.processor.ProcessesServiceOrder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ProcessingServiceOrderService {

    private final ServiceOrderService service;
    private final List<ProcessesServiceOrder> processesServiceOrder;

    public void processingOrders() {
        log.info("Consultando ordens de serviço cadastradas.");
        List<ServiceOrder> list = service.findAllServiceOrders();
        list.stream()
                .forEach(serviceOrder -> {
                    processesServiceOrder.stream()
                            .filter(processes -> processes.supports(serviceOrder.getStatusType()))
                            .findFirst()
                            .ifPresent(processes -> processes.sues(serviceOrder));
                });
    }
}
