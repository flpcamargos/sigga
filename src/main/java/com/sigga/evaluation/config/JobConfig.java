package com.sigga.evaluation.config;

import com.sigga.evaluation.job.ProcessingJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfig {

    @Bean
    public JobDetail jobADetails() {
        return JobBuilder.newJob(ProcessingJob.class).withIdentity("pollJob")
                .storeDurably().build();
    }

    @Bean
    public Trigger trigger(JobDetail details) {
        return TriggerBuilder.newTrigger().forJob(details)
                .withIdentity("pollJobtrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0/5 0 ? * * *"))
                .build();
    }

}
