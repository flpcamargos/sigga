package com.sigga.evaluation.service;

import com.sigga.evaluation.builder.AddressDTOBuilder;
import com.sigga.evaluation.builder.ServiceOrderDTOBuilder;
import com.sigga.evaluation.client.address.AddressClient;
import com.sigga.evaluation.exception.ContactTypeValidationException;
import com.sigga.evaluation.repository.ServiceOrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ServiceOrderServiceTest {

    @Mock
    private ServiceOrderRepository repository;

    @Mock
    private AddressClient client;

    private ServiceOrderService service;

    @Before
    public void init() {
        service = new ServiceOrderService(new ModelMapper(),
                repository,
                client);
    }

    @Test
    public void shouldTestCreateServiceOrderSucess() {
        when(client.getAddress("91370170"))
                .thenReturn(AddressDTOBuilder.getAddressDTO());
        service.createServiceOrder(ServiceOrderDTOBuilder.getServiceOrderDTO());
        verify(repository).save(any());
        verify(client).getAddress("91370170");
    }

    @Test
    public void shouldTestCreateServiceOrderContactTypeValidationException() {
        when(client.getAddress("91370170"))
                .thenReturn(AddressDTOBuilder.getAddressDTO());
        assertThrows(ContactTypeValidationException.class, () ->
                service.createServiceOrder(ServiceOrderDTOBuilder.getServiceOrderDTOWithContactTypeError()));
        verify(repository, never()).save(any());
        verify(client, never()).getAddress("91370170");
    }

}
