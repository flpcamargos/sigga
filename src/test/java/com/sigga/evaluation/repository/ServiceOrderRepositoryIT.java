package com.sigga.evaluation.repository;

import com.sigga.evaluation.model.ServiceOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql("classpath:insert_service_order.sql")
public class ServiceOrderRepositoryIT {
    @Autowired
    private ServiceOrderRepository repository;

    @Test
    public void shouldTestfindAll() {
        List<ServiceOrder> list = repository.findAll();
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals("08901432692", list.get(0).getTaxIdNumber());
    }
}
