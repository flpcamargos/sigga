package com.sigga.evaluation.builder;

import com.sigga.evaluation.dto.ServiceOrderDTO;
import com.sigga.evaluation.enumeration.ContactType;
import com.sigga.evaluation.enumeration.ServiceOrderType;

public class ServiceOrderDTOBuilder {
    public static ServiceOrderDTO getServiceOrderDTO() {
        return ServiceOrderDTO.builder()
                .contact("+55999999999")
                .contactType(ContactType.PHONE)
                .customerName("Teste One")
                .jobDescription("Teste One")
                .postalCode("91370170")
                .serviceOrderType(ServiceOrderType.CORRECTIVE_MAINTENANCE)
                .taxIdNumber("08901432692")
                .build();
    }

    public static ServiceOrderDTO getServiceOrderDTOWithContactTypeError() {
        ServiceOrderDTO dto = getServiceOrderDTO();
        dto.setContactType(ContactType.EMAIL);
        return dto;
    }
}
