package com.sigga.evaluation.builder;

import com.sigga.evaluation.dto.AddressDTO;

public class AddressDTOBuilder {
    public static AddressDTO getAddressDTO() {
        return AddressDTO.builder()
                .bairro("Vila Ipiranga")
                .cep("91370-170")
                .ddd("51")
                .ibge("4314902")
                .localidade("Porto Alegre")
                .logradouro("Avenida Dom Cláudio José Gonçalves Ponce de Leão")
                .siafi("8801")
                .uf("RS")
                .build();
    }
}
